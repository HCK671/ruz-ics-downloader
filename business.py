import requests
import os


class Downloader:

    def __init__(self, id, date_start, date_end):
        self.id = id
        self.date_start = date_start
        self.date_end = date_end

    def download(self):
        url = 'https://ruz.hse.ru/api/schedule/student/.ics?start=&finish=&lng=1'
        url = f'{url[:40]}{self.id}{url[40:51]}{self.date_start}{url[51:59]}{self.date_end}{url[59:65]}'
        r = requests.get(url)
        if not os.path.exists(os.path.abspath(f'./schedules')):
            os.mkdir(os.path.abspath(f'./schedules/'))
        if not os.path.exists(os.path.abspath(f'./schedules/{self.id}')):
            os.mkdir(os.path.abspath(f'./schedules/{self.id}'))
        with open(os.path.abspath(f'./schedules/{self.id}/RUZ-{self.id}.ics'), 'wb+') as f:
            f.write(r.content)
